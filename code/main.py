# from typing import Union
from fastapi import FastAPI, File, UploadFile 
import easyocr, uuid, json, os, io
from PIL import Image

app = FastAPI()


@app.get("/")
def test():
    """
        Just a simple hello world to verify the service is alive
    """
    return {"Hello": "World"}


@app.post("/sync_ocr/")
def analyze_file_sync(binary_file: UploadFile, extension: str, language: str):
    """
        Will accept image binary file as body input and extension and language (2 letters)\n
        as url params.\n
        will give a row full text ocr with box area and confidence as output (JSON format)
    """
    reader = easyocr.Reader([language], gpu=False)
    body_ocr_result = []
    width, height = 0, 0
    status = "ok"
    error = ""

    try:
        # read image from UploadFile
        image = Image.open(binary_file.file)
        bytes_buffer = io.BytesIO()
        image.save(bytes_buffer,format=extension)

        # get image data
        width = image.width
        height = image.height
        ocr_rows = reader.readtext(bytes_buffer.getvalue())
        for row in ocr_rows:
            singel_row_data = {}
            corners = row[0]
            singel_row_data["position"] = {
                "top_left": {"x":int(corners[0][0]), "y":int(corners[0][1])},
                "top_right": {"x":int(corners[1][0]), "y":int(corners[1][1])},
                "bottom_right": {"x":int(corners[2][0]), "y":int(corners[2][1])},
                "bottom_left": {"x":int(corners[3][0]), "y":int(corners[3][1])},
            }
            singel_row_data["text"] = row[1]
            singel_row_data["confidence"] = row[2]
            body_ocr_result.append(singel_row_data)
    except Exception as ex:
        print(ex)
        status = "ko"
        error = str(ex)
        
    return {"status":status, "width":width, "height":height, "ocr": body_ocr_result, "error":error}