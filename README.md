# EasyOCR-Docker

Questo progetto punta a rendere veloce ed accessibile l'OCR di documenti a tutti.

È basato sulla libreria [EasyOCR](https://github.com/JaidedAI/EasyOCR) come motore di OCR e su [FastAPI](https://fastapi.tiangolo.com/) come framework

---

---

### 🔨How to install

In order to use this software you need [Docker](https://www.docker.com/) and [docker-compose]([Use Docker Compose | Docker Documentation](https://docs.docker.com/get-started/08_using_compose/)) , (or similar like [Podman](https://podman.io/)).

You just need to run the docker-compose file, copy it on your server and run it with docker-compose

### 🚀How to use it

You can use OpenAPI page by typing `/docs` at the end of URL (as example http://127.0.0.1:8000/docs )

At the moment this software provide only a sync call to perform OCR.

To use it you need to perform a POST call at URL+`/sync_ocr/` and pass **extension** and **language** as parameters and **binary_file** as binary file on the body.

For extensions only PNG e JPG are tested for now.

For languages pls take a look in [EasyOCR Wiki](https://www.jaided.ai/easyocr/documentation/) .

Software will send you back a response like this of it's all ok

```json
{
    "status": "ok",
    "width": 312,
    "height": 471,
    "ocr": [
        {
            "position": {
                "top_left": {
                    "x": 41,
                    "y": 49
                },
                "top_right": {
                    "x": 69,
                    "y": 49
                },
                "bottom_right": {
                    "x": 69,
                    "y": 63
                },
                "bottom_left": {
                    "x": 41,
                    "y": 63
                }
            },
            "text": "Hello",
            "confidence": 0.9994687547555882
        },
        {
            "position": {
                "top_left": {
                    "x": 74,
                    "y": 40
                },
                "top_right": {
                    "x": 228,
                    "y": 40
                },
                "bottom_right": {
                    "x": 228,
                    "y": 64
                },
                "bottom_left": {
                    "x": 74,
                    "y": 64
                }
            },
            "text": "World:",
            "confidence": 0.6452324221258182
        }
    ],
    "error": ""
}
```

If status is ko, you can check the error field for more information.

### ✨Feature

There is a GET API to check the servic status, you can monitor it with services like Nagios or Uptime Kuma

Full-text OCR di immagini con posizione dei testi

---

---

### 📐TODO

- Wiki

- Better log system

- Automathic tests

- Libraries autoupdate

- GPU support

- Async call